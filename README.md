# eTrust - homepage frontend

## Setup na serwerze

1. Pobranie repozytorium 
2. Przejście do katalogu repozytorium
3. Wykonanie komend (nie na serwerze, ale lokalnie)
   - `yarn install`
   - `yarn build --mode="production"`
4. Przesłanie utworzonego w wyniku działania powyższej komendy katalogu "dist", do katalogu domeny.
5. Konfiguracja nginxa (patrz niżej **Setup nginxa**)
   - dodanie `gzip_static on;` w celu serwowania skompresowanych plików
   - dodanie `try_files $uri $uri/ /index.html;`
   - dodanie `root /www-data/dev-01.etrust.pl/etrust_new/dist;`
     - `/www-data/dev-01.etrust.pl/etrust_new/dist` to ścieżka bezwzględna do dist

## Lokalny setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build --mode="production"
```

## Modyfikacja treści

Treść najważniejszych meta tagów jest wstrzykiwana za pomocą pluginu, który jest częścią vue cli. Możliwa jest edycja tych treści w pliku /vue.config.js. Jeśli czegoś nie ma w tym pliku, to prawdopodobnie jest zahardkodowane.

Treści takie jak:
- realizacje
- media społecznościowe
- usługi
- zakładki głównego menu
- powiązane grafiki
można edytować przez zmianę /src/store/index.js

Inne treści + miejsce
- liczby (rok założenia, ZAKOŃCZONYCH PROJEKTÓW itp) -> /src/components/home/Numbers.vue
- biura (adresy, nr tel, email, marker) -> /src/views/Contact.vue 

Po wprowadzonych zmianach konieczne jest zbudowanie projektu.

## Setup nginxa

Przykładowa konfiguracja
```
server {
    listen       80; // http - 80, https - 443
    server_name  etrust.dev-01.etrust.pl; 

    charset utf-8;
    access_log  /var/log/nginx/access.etrust.dev-01.log  main;
    error_log /var/log/nginx/error.etrust.dev-01.log warn;

    root   /www-data/dev-01.etrust.pl/etrust_new/dist; // ścieżka do katalogu z dist
    index   index.html index.htm;
    gzip_static on; // serwowanie skompersowanych plików

    location / {
        root  /www-data/dev-01.etrust.pl/etrust_new/dist; // ścieżka do katalogu z dist
        try_files $uri $uri/ /index.html; // obowiązkowe dla vue
    }

    location ~ /\.ht {
      deny  all;
    }
}
```