const PrerenderSPAPlugin = require('prerender-spa-plugin');
const Renderer = PrerenderSPAPlugin.PuppeteerRenderer;
const path = require('path');

let spaData = {
  routes: [
    '/',
    '/rodo',
    '/realizacje',
    '/firma',
    '/realizacje/system-wsparcia-sprzedazy',
    '/realizacje/system-eventowy',
    '/realizacje/akcja-picia-wody',
    '/realizacje/platforma-probek',
    '/realizacje/uniwersyteckie-centrum-kliniczne-monitor-badan-klinicznych-korban',
    '/realizacje/uniwersyteckie-centrum-kliniczne-monitor-badan-klinicznych',
    // '/kontakt',
  ],
  meta: [
    {
      route: '/realizacje/system-eventowy',
      title: 'Siemens - System eventowy',
      description: 'System eventowy pozwala obsługiwać realizowane przez Siemens konferencje...',
      og_image: '/img/og/system-eventowy.jpg',
    },
    {
      route: '/realizacje/system-wsparcia-sprzedazy',
      title: 'Xella - System wsparcia sprzedaży',
      description: 'Xella Expert Budowy - program partnerski skierowany do projektantów...',
      og_image: 'img/og/system-wsparcia-sprzedazy.jpg',
    },
    {
      route: '/realizacje/akcja-picia-wody',
      title: 'Wybieram wodę - Ogólnopolska akcja promująca picie wody w szkołach',
      description: 'Ogólnopolska społeczna kampania edukacyjna „Wybieram wodę” to wspólna...',
      og_image: '/img/og/akcja-picia-wody.jpg',
    },
    {
      route: '/realizacje/platforma-probek',
      title: 'Saint Globain - Platforma do zamawiania próbek kolorystycznych Weber',
      description: 'Platforma do zamawiana próbek kolorystycznych Weber pozwala obsługiwać zamówienia...',
      og_image: '/img/og/platforma-probek.jpg',
    },
    {
      route: '/realizacje/monitor-badań',
      title: 'Uniwersyteckie Centrum Kliniczne - Monitor Badań Klinicznych',
      description: 'Monitor Badań Klinicznych to system zdalnego dostępu do pełnych danych pacjentów UCK, którzy wyrazili...',
      og_image: '/img/og/monitor-badań.jpg',
    },
    {
      route: '/realizacje/system-korban',
      title: 'Uniwersyteckie Centrum Kliniczne - Monitor Badań Klinicznych',
      description: 'System przeznaczony dla personelu Klinicznego Oddziału Ratunkowego. System...',
      og_image: '/img/og/system-korban.jpg',
    },
  ]
}

module.exports = {
  publicPath: process.env.BASE_URL,
  filenameHashing: true, //hash everything
  chainWebpack: config => {
    config
      .plugin('html')
      .tap(args => {
        args[0] = {
          ...args[0],
          hash: true, // html hash
          title: 'e-Trust Sp. z o.o. - Technologie internetowe',
          description: 'Agencja specjalizująca się w rozwiązaniach dla różnych obszarów biznesu. Działamy od 2006r. w projektach o zasięgu międzynarodowym. Specjalizujemy się w realizacji rozwiązań dedykowanych. Systemy medyczne, korporacyjne, serwisy firmowe oraz produktowe. Integracje platform, komunikacje systemów. Gwarantujemy responsywność i nowoczesność rozwiązań.',
          author: 'e-Trust Sp. z o.o.',
          keywords: 'strony internetowe, sysemu www, e-commerce, strony www, wysyłki mailingowe, strony produktowe, systemy zarządznia, technologie mobilne, audyt danych sobowych, szkolenia GIODO',

          og_url: process.env.SITE_URL,
          og_image: process.env.SITE_URL+'/img/OBRAZ-LINK.jpg',
          twitter_image: process.env.SITE_URL+'/img/OBRAZ-LINK.jpg',

          // og_title: 'e-Trust Sp. z o.o. - Technologie internetowe',
          // og_site_name: 'e-Trust Sp. z o.o. - Technologie internetowe',
          // og_description: 'Agencja specjalizująca się w rozwiązaniach dla obszaru mediów elektronicznych. Działamy od 2006r. w projektach o zasięgu międzynarodowym. Realizujemy systemy dedykowane oraz OpenSource. WebDesign, Systemy korporacyjne, Serwisy firmowe i produktowe. Gwarantujemy responsywność i nowoczesność rozwiązań.',

          // twitter_site: 'http://www.etrust.pl/',
          // twitter_title: 'e-Trust Sp. z o.o. - Technologie internetowe',
          // twitter_description: 'Agencja specjalizująca się w rozwiązaniach dla obszaru mediów elektronicznych. Działamy od 2006r. w projektach o zasięgu międzynarodowym. Realizujemy systemy dedykowane oraz OpenSource. WebDesign, Systemy korporacyjne, Serwisy firmowe i produktowe. Gwarantujemy responsywność i nowoczesność rozwiązań.',
          
        }

        return args
    })

    const svgRule = config.module.rule('svg');

    svgRule.uses.clear();
      
    svgRule
      .oneOf('inline')
      .resourceQuery(/inline/)
      .use('vue-loader')
      .loader('vue-loader-v16') // or `vue-loader-v16` if you are using a preview support of Vue 3 in Vue CLI vue-template-compiler
      .end()
      .use('vue-svg-loader')
      .loader('vue-svg-loader')
      .end()
      .end()
      .oneOf('external')
      .use('file-loader')
      .loader('file-loader')
      .options({
        name: 'assets/[name].[hash:8].[ext]',
      });

    config.module
      .rule("images")
      .use("url-loader")
      .loader("url-loader")
      .tap((options) => {
        options.fallback.options.name = "img/[name].[hash:8].[ext]"
        return options
      })

    if (process.env.NODE_ENV === 'production') {
      config
        .plugin('webp')
        .use('imagemin-webp-webpack-plugin')
        .tap(args => [...args, { overrideExtension: true, detailedLogs: false, config: [{
          test: /\.(jpe?g|png)/,
          options: {
            quality:  80
          }}] }])
      config
        .plugin("prerender-spa-plugin")
        .use(PrerenderSPAPlugin)
        .init(Plugin => new Plugin({
            staticDir: path.join(__dirname, 'dist'),
            routes: spaData.routes,
            postProcess(renderedRoute) {
                renderedRoute.html = renderedRoute.html
                  .replace('id="app"', 'id="app" data-server-rendered="true"')
                  .replace(/\s+/g, ' ')
                  .trim();

                let index = spaData.meta.findIndex((i) => {if(renderedRoute.route == i.route) return true;}),
                seo = index >= 0 ? spaData.meta[index] : false;

                const changeMeta = function(html, property, content){
                  let re = new RegExp("<meta property=\""+property+"\" content=\"(.*?)\">");
                  return html.replace(re, '<meta property="'+property+'" content="'+content+'">');
                }
                
                if(seo){
                  renderedRoute.html = changeMeta(renderedRoute.html, 'og:image', process.env.SITE_URL + seo.og_image);
                  renderedRoute.html = changeMeta(renderedRoute.html, 'og:title', seo.title);
                  renderedRoute.html = changeMeta(renderedRoute.html, 'og:description', seo.description);
                }

                return renderedRoute;
            },
            minify: {
              collapseBooleanAttributes: true,
              collapseWhitespace: true,
              decodeEntities: true,
              keepClosingSlash: true,
              sortAttributes: true
            },
            renderer: new Renderer({
                renderAfterDocumentEvent: 'render-event',
                renderAfterElementExists: '#app',
                // timeout: 0,
                maxConcurrentRoutes: 10,
                renderAfterTime: 10000,
                headless: true,
                onlyProduction: true,
                minify: {
                    collapseBooleanAttributes: true,
                    collapseWhitespace: true,
                    decodeEntities: true,
                    keepClosingSlash: true,
                    sortAttributes: true
                },
            }),
        }));
      config
        .plugin('brotli-compression')
      config
        .plugin('gzip-compression')
    }
  },
  pluginOptions: {
    sitemap: {
        baseURL: 'https://etrust.pl',
        outputDir: './public',
        trailingSlash: false,
        pretty: true,
        defaults: {
            lastmod: new Date(1612117180323),
            changefreq: 'weekly',
            priority: 1
        },
        routes: [
          {
            path: '/',
            meta: {
              sitemap: {
                lastmod: 'February 2021',
                priority: 1,
                changefreq: 'monthly'
              }
            }
          },
          {
            path: '/firma',
            meta: {
              sitemap: {
                lastmod: 'February 2021',
                priority: 1,
                changefreq: 'monthly'
              }
            }
          },
          {
            path: '/uslugi',
            meta: {
              sitemap: {
                lastmod: 'February 2021',
                priority: 0.7,
                changefreq: 'monthly'
              }
            }
          },
          {
            path: '/realizacje',
            meta: {
              sitemap: {
                lastmod: 'February 2021',
                priority: 0.7,
                changefreq: 'monthly'
              }
            }
          },
          {
            path: '/kontakt',
            meta: {
              sitemap: {
                lastmod: 'February 2021',
                priority: 0.9,
                changefreq: 'monthly'
              }
            }
          },
        ]
    },
    compression:{
      brotli: {
        filename: '[path].br[query]',
        algorithm: 'brotliCompress',
        include: /\.(js|css|html|svg|json)(\?.*)?$/i,
        compressionOptions: {
          level: 11,
        },
        minRatio: 0.8,
      },
      gzip: {
        filename: '[path].gz[query]',
        algorithm: 'gzip',
        include: /\.(js|css|html|svg|json)(\?.*)?$/i,
        minRatio: 0.8,
      }
    },
    'style-resources-loader': {
        preProcessor: 'scss',
        patterns: []
    },
    // css: {
    //   loaderOptions: {
    //     css: {
    //       modules: true,
    //       localIdentName: '[name]-[hash:6]',
    //     }
    //   }
    // }
  },
  configureWebpack:{
    performance: {
      hints: false
    },
    optimization: {
      splitChunks: {
        minSize: 10000,
        maxSize: 250000,
      }
    }
  }
}
