import Home from '@/views/Home.vue'

export const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
        title: null,
        canonoical: null
    }
  },
  {
    path: '/firma',
    name: 'About',
    // component: About,
    component: () => import(/* webpackChunkName: "about" */ '@/views/About.vue'),
    meta: {
        title: null,
        canonoical: null
    }
  },
  {
    path: '/kontakt',
    name: 'Contact',
    // component: Contact,
    component: () => import(/* webpackChunkName: "contact" */ '@/views/Contact.vue'),
    meta: {
        title: null,
        canonoical: null
    }
  },
  {
    path: '/realizacje/:slug?',
    name: 'RealizationsView',
    // component: RealizationsView,
    component: () => import(/* webpackChunkName: "realizations" */ '@/views/RealizationsView.vue'),
    meta: {
        title: null,
        canonoical: null
    }
  },
  {
    path: '/zapytania-ofertowe',
    name: 'Requests',
    // component: Requests,
    component: () => import(/* webpackChunkName: "requests" */ /* webpackChunkName: "requests" */ '@/views/Requests.vue'),
    meta: {
        title: null,
        canonoical: null
    }
  },
  {
    path: '/uslugi',
    name: 'Services',
    // component: Services,
    component: () => import(/* webpackChunkName: "noprefetch-[request]" */ /* webpackChunkName: "services" */ '@/views/Services.vue'),
    meta: {
        title: null,
        canonoical: null
    }
  },
  {
    path: '/rodo',
    name: 'Rodo',
    // component: Services,
    component: () => import(/* webpackChunkName: "noprefetch-[rodo]" */ /* webpackChunkName: "rodo" */ '@/views/Rodo.vue'),
    meta: {
        title: null,
        canonoical: null
    }
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'Error',
    component: () => import(/* webpackChunkName: "error-page" */ '@/views/ErrorPage.vue'),
    props: { errorCode: 404 }
    // redirect: { name: 'Home' },
  }
]