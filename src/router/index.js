import { createRouter, createWebHistory } from 'vue-router'
import VueScrollTo from 'vue-scrollto'

import { routes } from './routes.js'

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  mode: 'history', // history
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (to.hash) {
      VueScrollTo.scrollTo(to.hash, 700);
      return { 
        selector: to.hash,
        behavior: 'smooth'
      }
    } else if (savedPosition) {
      return savedPosition;
    } else {
      return { 
        left: 0,
        top: 0,
        behavior: 'smooth' 
      }
		}
	}
})

export default router
