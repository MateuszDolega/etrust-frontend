import { createApp } from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'

process.env.VUE_APP_VERSION = require('./../package.json').version



createApp(App)
.mixin({
    methods: {
        webp: function (path) {
            // return process.env.NODE_ENV  == 'production' && path.slice(path.length - 4, path.length) != 'webp' ? (path + '.webp') : path;
            return process.env.NODE_ENV  == 'production' && path.slice(path.length - 4, path.length) != 'webp' ? (path.slice(0, path.length - 4) + '.webp') : path;
        },
        stringToSlug: function (str, separator='-') {
            return str
                .toString()
                .normalize('NFD')                   // split an accented letter in the base letter and the acent
                .replace(/[\u0300-\u036f]/g, '')   // remove all previously split accents
                .toLowerCase()
                .trim()
                .replace(/[^a-z0-9 ]/g, '')   // remove all chars not letters, numbers and spaces (to be replaced)
                .replace(/\s+/g, separator);
          }
    },
})
.use(store)
.use(router)
.mount('#app')
